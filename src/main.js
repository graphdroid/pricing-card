const monthlyBtn = document.getElementById('monthly-btn')
const annuallyBtn = document.getElementById('annually-btn')

const monthlyPrice = document.querySelectorAll('.monthly-price')
const annuallyPrice = document.querySelectorAll('.annually-price')
const savesInfo = document.querySelectorAll('.saves-info')

const cards = document.querySelectorAll('.card')

monthlyBtn.addEventListener('click', () => {
    annuallyBtn.classList.remove('bg-primary', 'border-primary', 'text-light', 'fw-bold')
    monthlyBtn.classList.remove('border-dark')
    monthlyBtn.classList.add('bg-primary', 'text-light', 'border-primary', 'fw-bold')
    monthlyPrice.forEach(e => {
        e.classList.remove('d-none')
    })
    annuallyPrice.forEach(e => {
        e.classList.add('d-none')
    })
    savesInfo.forEach(e => {
        e.classList.add('d-none')
    })
})

annuallyBtn.addEventListener('click', () => {
    monthlyBtn.classList.remove('bg-primary', 'border-primary', 'text-light', 'fw-bold')
    annuallyBtn.classList.remove('border-dark')
    annuallyBtn.classList.add('bg-primary', 'text-light', 'border-primary', 'fw-bold')
    monthlyPrice.forEach(e => {
        e.classList.add('d-none')
    })
    annuallyPrice.forEach(e => {
        e.classList.remove('d-none')
    })
    savesInfo.forEach(e => {
        e.classList.remove('d-none')
    })
})

cards.forEach(e => {
    e.addEventListener('mouseover', () => {
        e.classList.add('shadow-lg')
        e.style.cursor="pointer"
    })
})

cards.forEach(e => {
    e.addEventListener('mouseout', () => {
        e.classList.remove('shadow-lg')
    })
})