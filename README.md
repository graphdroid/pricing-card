# **Responsive pricing card**

The pricing card project was made for the purpose of getting selected in the internship or permanent job as a Front end developer in Charpstar.

The employer has the requirement in which we the candidates have to use the HTML5, CSS3, JavaScript and Bootstrap 5 Framework for the project. We were allowed to use any image or icons in the project but we have to make atleast three pricing tiers.
***
<br>

## **Resources**


<br>

### **_Bootstrap 5.0 CDN_**

``` HTML 
<!-- CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<!-- Javascript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
```

### **_Fontawesome_**
I have downloaded some of the free fontawesome icons in the SVG format and modified it as per the requriement of the project.

The link below will direct you to the fontawesome icons which I have used in the project:

https://fontawesome.com/icons/check?s=solid&f=classic

https://fontawesome.com/icons/xmark?s=solid&f=classic
***
<br>

## **License**

MIT License